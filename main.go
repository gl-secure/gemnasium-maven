package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/convert"
)

func init() {
	log.SetFormatter(&logutil.Formatter{Project: "gemnasium-maven"})
}

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Gemnasium analyzer for GitLab Dependency-Scanning"
	app.Author = "GitLab"

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert.Convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

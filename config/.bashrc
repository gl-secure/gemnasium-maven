#!/bin/bash -l

# inflate extracts a given zstd tar archive to a given directory.
# It removes the file and is a no-op after being executed.
function inflate() {
  local file=$1
  local to_dir=$2
  if [ -f "$file" ]; then
    tar --use-compress-program zstd -xf "$file" -C "$to_dir"
    rm "$file"
  fi
}

# switch_to switches to a given tool and version
# Example: switch_to 'java' '11'
#
# The above command will switch to the adopt-openjdk-11.0.7+10 version of java
function switch_to() {
  local tool=$1
  local given_version=$2
  local tool_versions
  local selected_version

  # fetch line matching tool, ignore the first word and use 'read' to turn space-delimited words into an array
  IFS=" " read -r -a tool_versions <<< "$(grep "$tool" "$HOME/.tool-versions" | cut -d ' ' -f2-)"

  # if no match is found, default to first entry in .tool-versions file
  default_version=${tool_versions[0]}

  for tool_version in "${tool_versions[@]}"; do
      if [[ $tool_version = $given_version* ]]; then
          selected_version=$tool_version
          break
      fi
  done

  # no match found
  if [[ -z ${selected_version:-} ]]; then
      echo "Unable to find ${tool} version '${given_version}', defaulting to version '${default_version}'"
      selected_version=$default_version
  fi

  echo "Using ${tool} version '${selected_version}'"
  asdf shell "$tool" "$selected_version"
}

function enable_dev_mode() {
  unset HISTFILESIZE
  unset HISTSIZE
  export EDITOR=vim
  export LOG_LEVEL=debug
  set -o vi
  apt-get update -y
  apt-get install -y --no-install-recommends vim less shellcheck
}

inflate /opt/asdf.tar.zst /opt

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/completions/asdf.bash"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash
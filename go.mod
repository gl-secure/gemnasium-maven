module gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/spiegel-im-spiegel/errs v0.4.0 // indirect
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.10.3
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 v2.11.0
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

go 1.13

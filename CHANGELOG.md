# Gemnasium Maven analyzer changelog

## v2.16.1
- Fix bug causing failure when run in docker-in-docker mode (!58)

## v2.16.0
- Update logging to be standardized across analyzers (!55)

## v2.15.0
- Add `DS_JAVA_VERSION` var to allow changing java version (!53)

## v2.14.2
- Fix link to advisory (!54)

## v2.14.1
- Fix Gradle Kotlin build scripts not being detected (!40)

## v2.14.0
- Add `ADDITIONAL_CA_CERT_BUNDLE` to java keystore (!50)

## v2.13.0
- Add `GRADLE_CLI_OPTS` environment variable (!46)
- Add `SBT_CLI_OPTS` environment variable (!46)

## v2.12.0
- Update gemnasium to `v2.10.0` which adds support for vulnerability Severity levels

## v2.11.0
- Add `id` field to vulnerabilities in JSON report (!44)

## v2.10.0
- Make `gemnasium-maven-plugin` a build-time install in the analyzer image (!42)
- Make `gemnasium-gradle-plugin` a build-time install in the analyzer image (!43)
- Make `sbt-dependency-graph` plugin a build-time install in the analyzer image (!45)

## v2.9.1
- Specify maven and java version for base docker image (!36)

## v2.9.0
- Add support for custom CA certs (!33)

## v2.8.0
- Bump gemnasium-maven-plugin to v0.4.0 (!35)

## v2.7.0
- Use `MAVEN_CLI_OPTS` with all invocations of maven (@fcbrooks) (!21)

## v2.6.0
- Add support for projects built with sbt (!20)

## v2.5.0
- Add support for projects built with gradle (!17)

## v2.4.0
- Add `MAVEN_CLI_OPTS` variable with `-DskipTests --batch-mode` default (!16)

## v2.3.0
- Use gemnasium-db git repo instead of the Gemnasium API (!15)

## v2.2.4
- Run `mvn install` before analysis to fix multi-modules support when using internal dependencies between modules

## v2.2.3
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!13)

## v2.2.2
- Fix dependency list, include dependency files which do not have any vulnerabilities (!11)

## v2.2.1
- Sort the dependency files and their dependencies (!9)

## v2.2.0
- List the dependency files and their dependencies (!8)

## v2.1.2
- Bump common to v2.1.6
- Bump gemnasium to v2.1.2

## v2.1.1
- Fix multi-modules support

## v2.1.0
- Bump common to v2.1.4, introduce remediations
- Bump gemnasium to v2.1.1, introduce stable report order

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
